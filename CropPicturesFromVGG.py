import argparse
import os
import pandas as pd
import numpy as np
from PIL import Image, ImageDraw, ImageChops


def makePolygon(xList, yList):
    polygon=[]
    for value in range(len(xList)):
        tuple=(xList[value], yList[value])
        polygon.append(tuple)
    return polygon


def get_filename(path):
    base = os.path.basename(path)
    return base


def find_file(dir_path, filename):
    for file in os.listdir(dir_path):
        if get_filename(file) == filename:
            return os.path.join(dir_path, file)
    return None


def main():
    parser = argparse.ArgumentParser(description='VGG annotation to mask.')
    parser.add_argument('--input_csv', dest='input_csv', help='')
    parser.add_argument('--originals', dest='originals', help='')
    parser.add_argument('--output_dir', dest='output_dir', help='')

    args = parser.parse_args()
    data = pd.read_csv(args.input_csv)
    for index, row in data.iterrows():
        filename = row['#filename']
        orig_path = find_file(args.originals, filename)
        if orig_path is None:
            print(f'skip {filename}')
            continue
        print(f'process {filename}')
        original = Image.open(orig_path)
        mask = Image.new('RGB', original.size, color=(0,0,0))
        for i, polygon_row in data[data['#filename'] == filename].iterrows():
            if eval(polygon_row['region_attributes'])['type'] != 'pole':
                continue
            polygon = makePolygon(
                eval(polygon_row['region_shape_attributes'])['all_points_x'], 
                eval(polygon_row['region_shape_attributes'])['all_points_y'])
            mask_draw = ImageDraw.Draw(mask)
            mask_draw.polygon(polygon, fill=(255,255,255))
        save_path = os.path.join(args.output_dir, filename)
        mask.save(save_path)

if __name__ == "__main__":
    main()

