function Rename-Files {
    Param([string]$dirPath,[int]$startIndex)
    $files = Get-ChildItem $dirPath
    for ($i=$startIndex; $i -lt $files.Count; $i++) {
        $guid = New-Guid
	    Rename-Item $files[$i].FullName "$guid.jpg" 
    }
    $files = Get-ChildItem $dirPath
    for ($i=$startIndex; $i -lt $files.Count; $i++) {
	    Rename-Item $files[$i].FullName "$i.jpg" 
    }
}